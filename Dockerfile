# Ericsson - TIM Solutions - MTPI Internacional
# William Melchior Jablonski - william.jablonski@mtpi.com.br
# 03/03/2017 BRT

## create machine
# sudo docker build -t 'name:jenkins-ericsson' --memory 2048 -t jenkins-ericsson .
## run machine
#  sudo docker run -p 8080:8080 -p 50000:50000 jenkins-ericsson
## get port to ssh connection
# sudo docker port jenkins-ericsson 22
## ssh connect into docker 
# docker attach jenkins-ericsson
## zip machine
# sudo ddocker export amazing_davinci -o .

FROM jenkins

ENV JAVA_OPTS="-XX:PermSize=1024m -XX:MaxPermSize=512m"

# if we want to install via apt
USER root
RUN apt-get update

# install git
RUN apt-get install -y git

# install sshpass
RUN apt-get install -y sshpass

# Install maven 
RUN apt-get install -y maven 

# Install java 8 oracle
#RUN apt-get install -y python-software-properties
#RUN apt-get install -y software-properties-common
#RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections 
#RUN apt-get update 
#RUN apt-get install oracle-java8-installer
#RUN rm -rf /var/lib/apt/lists/* 
#RUN rm -rf /var/cache/oracle-jdk8-installer
# Define commonly used JAVA_HOME variable
#ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

# Install java 8 OpenJDK
RUN apt-get install -y openjdk-7-jdk

# Install firefox 49.01
RUN apt-get remove -y firefox 
RUN cd /usr/local 
RUN wget http://ftp.mozilla.org/pub/firefox/releases/49.0.1/linux-x86_64/en-US/firefox-49.0.1.tar.bz2 
RUN tar xvjf firefox-49.0.1.tar.bz2 
RUN ln -s /usr/local/firefox/firefox /usr/bin/firefox

# Install Chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - 
RUN sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' 
RUN apt-get update 
RUN apt-get install -y google-chrome-stable 

# Install PhantonJs
RUN apt-get install -y build-essential chrpath libssl-dev libxft-dev apt-utils
RUN apt-get install -y libfreetype6 libfreetype6-dev
RUN apt-get install -y libfontconfig1 libfontconfig1-dev
RUN cd ~
RUN export PHANTOM_JS="phantomjs-1.9.8-linux-x86_64"
RUN wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
RUN tar xvjf phantomjs-2.1.1-linux-x86_64.tar.bz2
RUN mv phantomjs-2.1.1-linux-x86_64 /usr/local/share
RUN ln -sf /usr/local/share/$PHANTOM_JS/bin/phantomjs /usr/local/bin

# Install GeckoDriver
RUN mkdir /var/lib/jenkins
RUN cd /var/lib/jenkins
RUN mkdir /var/lib/jenkins/automation_gecko_driver
RUN cd /var/lib/jenkins/automation_gecko_driver
RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.14.0/geckodriver-v0.14.0-linux64.tar.gz
RUN gunzip geckodriver-v0.14.0-linux64.tar.gz
# RUN rm var/lib/jenkins/automation_gecko_driver/geckodriver-v0.14.0-linux64.tar.gz
RUN chown -Rf jenkins:jenkins /var/lib/jenkins

# Install Chrome Driver
USER root
RUN apt-get install -y unzip
RUN wget -N http://chromedriver.storage.googleapis.com/2.10/chromedriver_linux64.zip -P ~/Downloads
RUN unzip ~/Downloads/chromedriver_linux64.zip -d ~/Downloads
RUN chmod +x ~/Downloads/chromedriver
RUN mv -f ~/Downloads/chromedriver /usr/local/share/chromedriver
RUN ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver
RUN ln -s /usr/local/share/chromedriver /usr/bin/chromedriver 

#CMD [ "sh", "-c", "echo /var/jenkins_home/secrets/initialAdminPassword" ]
#RUN echo /var/jenkins_home/secrets/initialAdminPassword

# drop back to the regular jenkins user - good practice
USER jenkins
